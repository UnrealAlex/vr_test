// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "XRMotionControllerBase.h"
#include "HandController.generated.h"


class UMotionControllerComponent;
class UPhysicsConstraintComponent;

UCLASS()
class VR_TEST_API AHandController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHandController();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMotionControllerComponent* MotionController;

	UPROPERTY()
	FVector HandClientLocation;

	UPROPERTY()
	FRotator HandClientRotation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	UStaticMeshComponent* ControllerMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	UPhysicsConstraintComponent* PhysicsConstraint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	USkeletalMeshComponent* HandMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	FName HandRootBoneName;

	UPROPERTY(Replicated)
	FVector_NetQuantize10 HandLocation;

	UPROPERTY(Replicated)
	FRotator HandRotation;

	void SetHand(FName SourceName);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

};
