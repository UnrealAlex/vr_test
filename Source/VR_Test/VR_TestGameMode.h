// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VR_TestGameMode.generated.h"

UCLASS(minimalapi)
class AVR_TestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AVR_TestGameMode();
};



