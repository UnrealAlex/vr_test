// Copyright Epic Games, Inc. All Rights Reserved.

#include "VR_TestCharacter.h"
#include "VR_TestProjectile.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/InputSettings.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/CharacterMovementComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AVR_TestCharacter

AVR_TestCharacter::AVR_TestCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(30.f, 85.0f);

	VROrigin = CreateDefaultSubobject<USceneComponent>(TEXT("VROrigin"));
	VROrigin->SetupAttachment(RootComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(VROrigin);

	// Create VR Controllers.
	MotionControllerR = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	MotionControllerR->MotionSource = FXRMotionControllerBase::RightHandSourceId;
	MotionControllerR->SetupAttachment(VROrigin);

	ControllerMeshR = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ControllerMeshR"));
	ControllerMeshR->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ControllerMeshR->SetupAttachment(MotionControllerR);

	PhysicsConstraintR = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("PhysicsConstraintR"));
	PhysicsConstraintR->SetupAttachment(ControllerMeshR);

	HandMeshR = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HandR"));
	HandMeshR->SetupAttachment(ControllerMeshR);
	
	
	MotionControllerL = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	MotionControllerL->SetupAttachment(VROrigin);

	ControllerMeshL = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ControllerMeshL"));
	ControllerMeshL->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ControllerMeshL->SetupAttachment(MotionControllerL);

	PhysicsConstraintL = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("PhysicsConstraintL"));
	PhysicsConstraintL->SetupAttachment(ControllerMeshL);

	HandMeshL = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HandL"));
	HandMeshL->SetupAttachment(ControllerMeshL);

	TeleportMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TeleportMesh"));
	TeleportMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	TeleportMesh->SetHiddenInGame(true);
	TeleportMesh->SetupAttachment(RootComponent);

}

void AVR_TestCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	PhysicsConstraintL->SetConstrainedComponents(ControllerMeshL, "", HandMeshL, HandLRootBoneName);
	PhysicsConstraintR->SetConstrainedComponents(ControllerMeshR, "", HandMeshR, HandRRootBoneName);

	if(HasAuthority())
	{
		TArray<AActor*> IgnoredActors;
		IgnoredActors.Add(this);

		TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
		ObjectTypes.Add(EObjectTypeQuery::ObjectTypeQuery_MAX);
		
		TeleportParams.bTraceComplex = true;
		TeleportParams.TraceChannel = ECollisionChannel::ECC_WorldStatic;
		TeleportParams.bTraceWithChannel = true;
		TeleportParams.ObjectTypes = ObjectTypes;
		TeleportParams.bTraceWithCollision = true;
		TeleportParams.ActorsToIgnore = IgnoredActors;
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AVR_TestCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AVR_TestCharacter::MoveForward);
}

void AVR_TestCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(GetLocalRole() >= ROLE_AutonomousProxy && IsLocallyControlled())
	{
		UpdateRControllerTransform(FTransform_NetQuantize(MotionControllerR->GetComponentTransform()));

		UpdateLControllerTransform(FTransform_NetQuantize(MotionControllerL->GetComponentTransform()));

		float ZOffset = (CharacterHeight - CameraComponent->GetRelativeLocation().Z) * -1.f;
		float XOffset = CameraComponent->GetRelativeLocation().X;
		float YOffset = CameraComponent->GetRelativeLocation().Y;

		BodyRotation = CameraComponent->GetRelativeRotation().Yaw;

		BodyLocalOffset = FVector(XOffset, YOffset, ZOffset);

		UpdateBodyTransform(BodyLocalOffset, BodyRotation);

		if(HasAuthority())
		{
			RControllerTransformReplicated = FTransform_NetQuantize(MotionControllerR->GetComponentTransform());
			LControllerTransformReplicated = FTransform_NetQuantize(MotionControllerL->GetComponentTransform());
		}
	}

	if(GetLocalRole() < ROLE_AutonomousProxy)
	{
		RHandClientTransform = UKismetMathLibrary::TInterpTo(RHandClientTransform, FTransform(RControllerTransformReplicated.Rotation, RControllerTransformReplicated.Location), DeltaTime, InterpSpeed);
		MotionControllerR->SetWorldTransform(RHandClientTransform);

		LHandClientTransform = UKismetMathLibrary::TInterpTo(LHandClientTransform, FTransform(LControllerTransformReplicated.Rotation, LControllerTransformReplicated.Location), DeltaTime, InterpSpeed);
		MotionControllerL->SetWorldTransform(LHandClientTransform);
	}

	UpdateTelepotrLocation();
}

void AVR_TestCharacter::ActivateTeleport()
{
	TeleportMesh->SetHiddenInGame(false);

	ActivateTeleportServer();

	if(HasAuthority())
	{
		bTeleportIsActive = true;
	}
}

void AVR_TestCharacter::DeactivateTeleport()
{
	TeleportMesh->SetHiddenInGame(true);

	DeactivateTeleportServer();

	if(HasAuthority())
	{
		bTeleportIsActive = false;
	}
}

void AVR_TestCharacter::ExecuteTeleport()
{
	ExecuteTeleportServer();

	DeactivateTeleport();
}

void AVR_TestCharacter::UpdateTelepotrLocation()
{
	if(HasAuthority())
	{
		if(bTeleportIsActive)
		{
			TeleportParams.StartLocation = MotionControllerR->GetComponentLocation();
			TeleportParams.LaunchVelocity = MotionControllerR->GetForwardVector() * 2000.f;
			
			FPredictProjectilePathResult Result;
			UGameplayStatics::PredictProjectilePath(this, TeleportParams, Result);

			TeleportLocation = Result.HitResult.Location;
			TeleportMesh->SetWorldLocation(TeleportLocation);
		}
	}
	else
	{
		FVector Location = UKismetMathLibrary::VInterpTo(TeleportMesh->GetComponentLocation(), TeleportLocation, GetWorld()->DeltaTimeSeconds, 15.f);
		TeleportMesh->SetWorldLocation(Location);
	}
}

void AVR_TestCharacter::ExecuteTeleportServer_Implementation()
{
	TeleportTo(TeleportLocation, FRotator(0.f, CameraComponent->GetComponentRotation().Yaw, 0.f));
}

void AVR_TestCharacter::DeactivateTeleportServer_Implementation()
{
	bTeleportIsActive = false;
}

void AVR_TestCharacter::ActivateTeleportServer_Implementation()
{
	bTeleportIsActive = true;
}

void AVR_TestCharacter::UpdateRControllerTransform_Implementation(FTransform_NetQuantize Transform)
{
	RControllerTransformReplicated = Transform;
	MotionControllerR->SetWorldTransform(FTransform(Transform.Rotation, Transform.Location));
}

void AVR_TestCharacter::UpdateLControllerTransform_Implementation(FTransform_NetQuantize Transform)
{
	LControllerTransformReplicated = Transform;
	MotionControllerL->SetWorldTransform(FTransform(Transform.Rotation, Transform.Location));
}

void AVR_TestCharacter::UpdateBodyTransform_Implementation(FVector_NetQuantize10 Offset, float Rotation)
{
	BodyRotation = Rotation;
	BodyLocalOffset = Offset;
}

void AVR_TestCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AVR_TestCharacter, RControllerTransformReplicated, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AVR_TestCharacter, LControllerTransformReplicated, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AVR_TestCharacter, BodyRotation, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AVR_TestCharacter, BodyLocalOffset, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AVR_TestCharacter, TeleportLocation, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AVR_TestCharacter, GripRAxis, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AVR_TestCharacter, GripLAxis, COND_SkipOwner);
}


void AVR_TestCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AVR_TestCharacter::SetGripRAxis(float Axis)
{
	GripRAxis = Axis;

	if(GetLocalRole() >= ROLE_AutonomousProxy && IsLocallyControlled())
	{
		UpdateGripRAxis(GripRAxis);
	}
	
}

void AVR_TestCharacter::SetGripLAxis(float Axis)
{
	GripLAxis = Axis;

	if(GetLocalRole() >= ROLE_AutonomousProxy && IsLocallyControlled())
	{
		UpdateGripLAxis(GripLAxis);
	}
	
}

void AVR_TestCharacter::UpdateGripRAxis_Implementation(float Axis)
{
	GripRAxis = Axis;
}

void AVR_TestCharacter::UpdateGripLAxis_Implementation(float Axis)
{
	GripLAxis = Axis;
}
