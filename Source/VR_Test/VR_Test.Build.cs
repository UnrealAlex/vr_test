// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class VR_Test : ModuleRules
{
	public VR_Test(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"OculusHMD",
				"OVRPlugin"
			});
	}
}
