// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Kismet/GameplayStatics.h"
#include "VR_TestCharacter.generated.h"


class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
class UMotionControllerComponent;
class UAnimMontage;
class USoundBase;

USTRUCT()
struct FTransform_NetQuantize
{
	GENERATED_BODY()

	UPROPERTY()
	FVector_NetQuantize10 Location;

	UPROPERTY()
	FRotator Rotation;
 
	FTransform_NetQuantize()
	{
		Location = FVector_NetQuantize10::ZeroVector;
		Rotation = FRotator::ZeroRotator;
	}

	FTransform_NetQuantize(FTransform Transform)
	{
		Location = Transform.GetTranslation();
		Rotation = Transform.Rotator();
	}

};

UCLASS(config=Game)
class AVR_TestCharacter : public ACharacter
{
	GENERATED_BODY()

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* CameraComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMotionControllerComponent* MotionControllerR;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMotionControllerComponent* MotionControllerL;
	
	UPROPERTY()
	FTransform RHandClientTransform;

	UPROPERTY()
	FTransform LHandClientTransform;

	UPROPERTY()
	bool bTeleportIsActive;

public:
	AVR_TestCharacter();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	USceneComponent* VROrigin;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	UStaticMeshComponent* ControllerMeshR;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	UPhysicsConstraintComponent* PhysicsConstraintR;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	USkeletalMeshComponent* HandMeshR;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	FName HandRRootBoneName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	UStaticMeshComponent* ControllerMeshL;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	UPhysicsConstraintComponent* PhysicsConstraintL;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	USkeletalMeshComponent* HandMeshL;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	FName HandLRootBoneName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	float InterpSpeed;

	UPROPERTY(Replicated)
	FTransform_NetQuantize RControllerTransformReplicated;

	UPROPERTY(Replicated)
	FTransform_NetQuantize LControllerTransformReplicated;

	UPROPERTY(BlueprintReadOnly, Replicated, Category="VRCharacter")
	float BodyRotation;

	UPROPERTY(BlueprintReadOnly, Replicated, Category="VRCharacter")
	FVector_NetQuantize10 BodyLocalOffset;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="VRCharacter")
	float CharacterHeight;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "VRCharacter")
	float GripRAxis;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "VRCharacter")
	float GripLAxis;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="VRCharacter")
	UStaticMeshComponent* TeleportMesh;
	
	UPROPERTY(BlueprintReadOnly, Replicated, Category="VRCharacter")
	FVector_NetQuantize10 TeleportLocation;
	
	UPROPERTY()
	FPredictProjectilePathParams TeleportParams;

protected:
	virtual void BeginPlay();

	/** Handles moving forward/backward */
	void MoveForward(float Val);
	
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category="VRCharacter")
	void ActivateTeleport();

	UFUNCTION(BlueprintCallable, Category="VRCharacter")
    void DeactivateTeleport();

	UFUNCTION(BlueprintCallable, Category="VRCharacter")
    void ExecuteTeleport();

	UFUNCTION(Server, Unreliable)
	void ActivateTeleportServer();

	UFUNCTION(Server, Unreliable)
    void DeactivateTeleportServer();

	UFUNCTION()
	void UpdateTelepotrLocation();

	UFUNCTION(Server, Unreliable)
	void ExecuteTeleportServer();

	UFUNCTION(Server, Unreliable)
	void UpdateRControllerTransform(FTransform_NetQuantize Transform);

	UFUNCTION(Server, Unreliable)
    void UpdateLControllerTransform(FTransform_NetQuantize Transform);

	UFUNCTION(Server, Unreliable)
    void UpdateBodyTransform(FVector_NetQuantize10 Offset, float Rotation);

	UFUNCTION(BlueprintCallable, Category = "VRCharacter")
	void SetGripRAxis(float Axis);

	UFUNCTION(BlueprintCallable, Category = "VRCharacter")
	void SetGripLAxis(float Axis);

	UFUNCTION(Server, Unreliable)
	void UpdateGripRAxis(float Axis);

	UFUNCTION(Server, Unreliable)
	void UpdateGripLAxis(float Axis);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

};

