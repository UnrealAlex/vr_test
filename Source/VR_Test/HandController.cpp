// Fill out your copyright notice in the Description page of Project Settings.


#include "HandController.h"
#include "MotionControllerComponent.h"
#include "Net/UnrealNetwork.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
AHandController::AHandController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MotionController"));
	MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
	RootComponent = MotionController;

	ControllerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ControllerMesh"));
	ControllerMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ControllerMesh->SetupAttachment(RootComponent);

	PhysicsConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("PhysicsConstraint"));
	PhysicsConstraint->SetupAttachment(ControllerMesh);

	HandMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Hand"));
	HandMesh->SetupAttachment(ControllerMesh);
}

// Called when the game starts or when spawned
void AHandController::BeginPlay()
{
	Super::BeginPlay();

	if(HasAuthority())
	{
		PhysicsConstraint->SetConstrainedComponents(ControllerMesh, "", HandMesh, HandRootBoneName);
	}
	
}

// Called every frame
void AHandController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(HasAuthority())
	{
		HandLocation = HandMesh->GetComponentLocation();
		HandRotation = HandMesh->GetComponentRotation();
	}
	else
	{
		HandClientLocation = UKismetMathLibrary::VInterpTo(HandClientLocation, HandLocation, DeltaTime, 100.f);
		HandClientRotation = UKismetMathLibrary::RInterpTo(HandClientRotation, HandRotation, DeltaTime, 15.f);
		
		HandMesh->SetWorldLocationAndRotationNoPhysics(HandClientLocation, HandClientRotation);
	}

}

void AHandController::SetHand(FName SourceName)
{
	MotionController->MotionSource = SourceName;
}

void AHandController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AHandController, HandLocation);
	DOREPLIFETIME(AHandController, HandRotation);
}

