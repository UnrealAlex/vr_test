// Fill out your copyright notice in the Description page of Project Settings.


#include "PunchingBag.h"


#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"

// Sets default values
APunchingBag::APunchingBag()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RootMesh"));
	RootMesh->SetupAttachment(RootComponent);

	PunchingBagMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PunchingBagMesh"));
	PunchingBagMesh->SetupAttachment(RootMesh);

	PhysicsConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("PhysicsConstraint"));
	PhysicsConstraint->SetupAttachment(RootMesh);
}

void APunchingBag::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(APunchingBag, PunchingBagLocation, COND_SimulatedOnly);
	DOREPLIFETIME_CONDITION(APunchingBag, PunchingBagRotation, COND_SimulatedOnly);
}

// Called when the game starts or when spawned
void APunchingBag::BeginPlay()
{
	Super::BeginPlay();

	if(HasAuthority())
	{
		PunchingBagMesh->SetSimulatePhysics(true);
		PhysicsConstraint->SetConstrainedComponents(RootMesh, "", PunchingBagMesh, "");
	}
	else
	{
		PunchingBagMesh->SetSimulatePhysics(false);
	}
}

// Called every frame
void APunchingBag::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(HasAuthority())
	{
		PunchingBagLocation = PunchingBagMesh->GetComponentLocation();
		PunchingBagRotation = PunchingBagMesh->GetComponentRotation();
	}
	else
	{
		PunchingBagClientLocation = UKismetMathLibrary::VInterpTo(PunchingBagClientLocation, PunchingBagLocation, DeltaTime, InterpSpeed);
		PunchingBagClientRotation = UKismetMathLibrary::RInterpTo(PunchingBagClientRotation, PunchingBagRotation, DeltaTime, InterpSpeed);
		
		PunchingBagMesh->SetWorldLocationAndRotation(PunchingBagClientLocation, PunchingBagClientRotation);
	}
}

