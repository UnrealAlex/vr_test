// Copyright Epic Games, Inc. All Rights Reserved.

#include "VR_TestGameMode.h"
#include "VR_TestHUD.h"
#include "VR_TestCharacter.h"
#include "UObject/ConstructorHelpers.h"

AVR_TestGameMode::AVR_TestGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AVR_TestHUD::StaticClass();
}
