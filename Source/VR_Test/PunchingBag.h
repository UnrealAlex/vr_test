// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"

#include "PunchingBag.generated.h"

UCLASS()
class VR_TEST_API APunchingBag : public AActor
{
	GENERATED_BODY()

	UPROPERTY()
	FVector_NetQuantize10 PunchingBagClientLocation;

	UPROPERTY()
	FRotator PunchingBagClientRotation;
	
public:	
	// Sets default values for this actor's properties
	APunchingBag();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="PunchingBag")
	UStaticMeshComponent* RootMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="PunchingBag")
	UStaticMeshComponent* PunchingBagMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="PunchingBag")
	UPhysicsConstraintComponent* PhysicsConstraint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="PunchingBag")
	float InterpSpeed;

	UPROPERTY(Replicated)
	FVector_NetQuantize10 PunchingBagLocation;

	UPROPERTY(Replicated)
	FRotator PunchingBagRotation;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

